#Simulate a Coin Toss Experiment


import random


num_flip = 0

def flip():
    if random.randint(0,1) == 1:
        return "heads"
    else:
        return "tails"




for i in range(10000):
    if flip() == "heads":
        num_flip = num_flip + 1
        while flip() == "heads":
            num_flip = num_flip + 1
        num_flip = num_flip + 1

    else:
        num_flip = num_flip + 1
        while num_flip == "tails":
            num_flip = num_flip + 1
        num_flip = num_flip + 1


ratio = num_flip/10000
print(f"The average number of flips per trial is {ratio}.")
                     
            
