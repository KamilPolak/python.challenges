from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

class PdfFileSplitter:


    def __init__(self, file_path):

        self.pdf_reader = PdfFileReader(file_path)
        self.writer1 = PdfFileWriter()
        self.writer2 = PdfFileWriter()


    def split(self, breakpoint):
        

        for n in self.pdf_reader.pages[:breakpoint]:
            self.writer1.addPage(n)

        for n in self.pdf_reader.pages[breakpoint:]:
            self.writer2.addPage(n)



    def write(self, filename):

        with Path(filename + "_1.pdf").open(mode = 'wb') as output:
            self.writer1.write(output)

        with Path(filename + "_2.pdf").open(mode = 'wb') as output:
            self.writer2.write(output)


splitter = PdfFileSplitter(r"C:\Users\kamil\Downloads\Pride_and_Prejudice.pdf")
splitter.split(150)
splitter.write("split")
