#List of lists

universities = [
['California Institute of Technology', 2175, 37704],
['Harvard', 19627, 39849],
['Massachusetts Institute of Technology', 10566, 40732],
['Princeton', 7802, 37000],
['Rice', 5879, 35551],
['Stanford', 19535, 40569],
['Yale', 11701, 40500]
]



def enrollment_stats(universities):

    student_enrollment = []
    tuition_fees = []

    for i in universities:
        student_enrollment.append(i[1])
        tuition_fees.append(i[2])

    return student_enrollment, tuition_fees

def mean(val):
    return sum(val)/len(val)


def median(val):
    val.sort()
    
    if len(val) % 2 == 1:
        
        center_val = int(len(val) / 2)
        return val[center_val]
    else:
        left_center_val = (len(val) - 1) // 2
        right_center_val = (len(val) + 1) // 2
        return mean([val[left_center_val], val[right_center_val]])


total = enrollment_stats(universities)

print("\n")
print("*****" * 6)
print(f"Total students:   {sum(total[0]):,}")
print(f"Total tuition:  $ {sum(total[1]):,}")
print(f"\nStudent mean:     {mean(total[0]):,.2f}")
print(f"Student median:   {median(total[0]):,}")
print(f"\nTuition mean:   $ {mean(total[1]):,.2f}")
print(f"Tuition median: $ {median(total[1]):,}")
print("*****" * 6)
print("\n") 
