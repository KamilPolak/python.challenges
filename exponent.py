#display the first number raised to the power of the second number

input1 = float(input("Enter a base: "))

input2 = int(input("Enter an exponent: "))

calc = input1 ** input2

print(f"{input1} to the power of {input2} = {calc} ")
