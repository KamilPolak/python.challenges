# Turn a user's input into leetspeak

input_text = input("Enter some text: ")

input_text = input_text.replace("a", "4")
input_text = input_text.replace("b", "8")
input_text = input_text.replace("e", "3")
input_text = input_text.replace("l", "1")
input_text = input_text.replace("o", "0")
input_text = input_text.replace("s", "5")
input_text = input_text.replace("t", "7")

print(input_text)
