#Track your investments



def invest(amount, rate, years):
    '''function which that tracks the growing amount of an investment over time'''

    for i in range(1, 1+years):
        amount = (1 + rate) * amount
        print(f"year {i}: ${amount:,.2f}")



#Ask user to input initial parameters
amount = float(input("Initial amount: "))
rate = float(input("Annual rate: "))
years = int(input("Number of years: "))

invest(amount, rate, years)

    
