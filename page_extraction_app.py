import easygui as gui
from PyPDF2 import PdfFileReader, PdfFileWriter

#1. Ask the user to select a PDF file to open.

input_path = gui.fileopenbox(title = "Select PDF", default = "*.pdf")

#2. If the user cancels the dialog, then exit the program.

if input_path is None:
    exit()

#3. Ask for a starting page number

num_page = gui.enterbox(msg = "Selcet starting page number", title = "Starting page number")

num_page = int(num_page)

#4. If the user does not enter a starting page number, exit the program.

if num_page is None:
    exit()


'''
5. Valid page numbers are positive integers. If the user enters an
invalid page number:
• Warn the user that the entry is invalid .
• Return to step 3..
'''
while num_page <= 0:
    gui.msgbox("Invalid page numer")
    num_page = gui.enterbox(msg = "Selcet starting page number", title = "Starting page number")


#6. Ask for a ending page number

num_page_end = gui.enterbox(msg = "Selcet ending page number", title = "Ending page number")

num_page_end = int(num_page_end)

#7. If the user does not enter a ending page number, exit the program.

if num_page_end is None:
    exit()


'''
8. Valid page numbers are positive integers. If the user enters an
invalid page number:
• Warn the user that the entry is invalid .
• Return to step 6..
'''
while num_page_end <= 0:
    gui.msgbox("Invalid page numer")
    num_page_end = gui.enterbox(msg = "Selcet ending page number", title = "Ending page number")

    if num_page_end is None:
        exit()


#9 Ask for the location to save the extracted pages

output_path = gui.filesavebox(title="Choose location to save your file", default="*.pdf")

#10 If the user does not select a save location, exit the program

if output_path is None:
    exit()

'''
11. If the chosen save location is the same as the input file path:
• Warn the user that they can not overwrite the input file.
• Return to step 9.
'''

while output_path == input_path:
    gui.msgbox(msg = "File name cannot be overriden", title = "Error name")
    output_path = gui.filesavebox(title=save_title, default="*.pdf")

    if output_path is None:
        exit()


'''
12. Perform the page extraction:
• Open the input PDF file.
• Write a new PDF file containing only the pages in the selected
page range.

'''

input_file = PdfFileReader(input_path)
output_pdf = PdfFileWriter(output_path)



for i in range(int(num_page) - 1, int(num_page_end)):
    page = input_file.getPage(i)
    output_pdf.addPage(page)
    


with open(output_path, "wb") as output_file:
    output_pdf.write(output_file)
    
