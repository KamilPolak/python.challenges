#Simulate an Election


from random import random


a_win = 0
b_win = 0

total_a_win = 0
total_b_win = 0

for i in range(10000):
    if random() < 0.87:
        a_win = a_win + 1
    else:
        b_win = b_win + 1

    if random() < 0.65:
        a_win = a_win + 1
    else:
        b_win = b_win + 1
            
    if random() < 0.17:
        a_win = a_win + 1
    else:
        b_win = b_win + 1

    if a_win > b_win:
        total_a_win = total_a_win + 1
    else:
        total_b_win = total_b_win + 1

prob_a = total_a_win/10000
prob_b = total_b_win/10000

print(f"Probability A wins: {prob_a}")
print(f"Probability B wins: {prob_b}")
