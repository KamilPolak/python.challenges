import PyPDF2

from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter


file_dir = Path(r"C:\Users\kamil\Downloads\scrambled.pdf")



def get_page_text(page):
    return page.extractText()

reader = PdfFileReader(str(file_dir))
writer = PdfFileWriter()



pages = list(reader.pages)
pages = pages.sort(key = get_page_text )



for i in reader.pages:
    rotation = i["/Rotate"]
    if rotation != 0:
        i.rotateCounterClockwise(rotation)
    writer.addPage(i)



with Path("outpur.pdf").open(mode = "wb") as output:
    writer.write(output)


