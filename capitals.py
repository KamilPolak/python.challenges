#Capital City Loop

import random

capitals_dict = {
'Alabama': 'Montgomery',
'Alaska': 'Juneau',
'Arizona': 'Phoenix',
'Arkansas': 'Little Rock',
'California': 'Sacramento',
'Colorado': 'Denver',
'Connecticut': 'Hartford',
'Delaware': 'Dover',
'Florida': 'Tallahassee',
'Georgia': 'Atlanta',
}

#Pick randomly state and capital form the dictionary
state, capital = random.choice(list(capitals_dict.items()))


#Ask user to provide name of the capital
user_input = input(f"Input the capital of the {state}: ").lower()


#Check
if user_input != capital.lower() or user_input == "exit":
    print(f"The capital of '{state}' is '{capital}'.")
    print("Goodbye")

else:
    print("Correct answer")
    print("Good bye")




