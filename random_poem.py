#Wax Poetic

import random

nouns =  ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
adjectives =  ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
prepositions =  ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]


def random_poem():
    """Create a randomly generated poem"""

    # Pick three random nouns
    n1 = random.choice(nouns)
    n2 = random.choice(nouns)
    n3 = random.choice(nouns)
    
    # Check if all nouns are unique
    while n1 == n2:
        n2 = random.choice(nouns)
    while n1 == n3 or n2 == n3:
        n3 = random.choice(nouns)

    # Pick three random verbs
    v1 = random.choice(verbs)
    v2 = random.choice(verbs)
    v3 = random.choice(verbs)

    #Check if all verbs are unique
    while v1 == v2:
        v2 = random.choice(verbs)
    while v1 == v3 or v2 == v3:
        v3 = random.choice(verbs)

    # Pick three random adjectives
    adj1 = random.choice(adjectives)
    adj2 = random.choice(adjectives)
    adj3 = random.choice(adjectives)

    #Check if all adjectives are unique
    while adj1 == adj2:
        adj2 = random.choice(adjectives)
    while adj1 == adj3 or adj2 == adj3:
        adj3 = random.choice(adjectives)

    # Pcik two random prepositions
    prep1 = random.choice(prepositions)
    prep2 = random.choice(prepositions)

    #Check if all prepositions are unique
    while prep1 == prep2:
        prep2 = random.choice(prepositions)

    # Pcik one adverb
    adv1 = random.choice(adverbs)


    #Define prefix
    if "aeiou".find(adj1[0]) != -1:  # First letter is a vowel
        article = "An"
    else:
        article = "A"

    # Create poem
    poem = (
        f"{article} {adj1} {n1}\n\n"
        f"{article} {adj1} {n1} {v1} {prep1} the {adj2} {n2}\n"
        f"{adv1}, the {n1} {v2}\n"
        f"the {n2} {v3} {prep2} a {adj3} {n3}"
    )

    return poem


poem = random_poem()
print(poem)
