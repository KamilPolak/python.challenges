import csv
from pathlib import Path



score_path = Path.home() / "Users" / "kamil" /'scores.csv'


with score_path.open(mode = 'r', encoding = 'utf-8') as file:
    reader = csv.DictReader(file)
    score = [row for row in reader]


best_score = {}

for i in score:
    name = i["name"]
    score = int(i["score"])

    if name not in best_score:
        best_score[name] = score
    else:
        if score > best_score[name]:
            best_score[name] = score



output = Path.home() / "output.csv"

with output.open(mode = "w", encoding = "utf-8") as file:
    fieldnames = ["name", "score"]
    writer = csv.DictWriter(file, fieldnames = fieldnames)

    writer.writerHeader()
    for i in best_score:
        dict_score = {"name": i, "best_score": best_score[i]}
        writer.writerow(dict_score)
