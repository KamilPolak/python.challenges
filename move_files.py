#Move All Image Files To a New Directory


from pathlib import Path

# Change this path to match the location on your computer
documents_dir = (
    Path.home() /
    "Dokumenty"/
    "my_folder"
    
)


images_dir = Path.home() / "images"
images_dir.mkdir(exist_ok=True)


for path in documents_dir.rglob("*.*"):
    if path.suffix.lower() in [".png", ".jpg", ".gif"]:
        path.replace(images_dir / path.name)
