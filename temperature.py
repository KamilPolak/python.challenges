#Convert Temperatures


def convert_cel_to_far(c):
    '''Convert Celsius to Fahrenheit'''
    far = c * (9/5) + 32
    return far


def convert_far_to_cel(f):
    '''Convert Fahrenheit to Celsius'''
    cel = (f - 32) * (5/9)
    return cel


#Ask user to input a Fahrenheit temperature
temp_far = input("Enter a temperature in degrees F: ")

#Print converted temperature in Celsius
print(f"{temp_far} degrees F = {convert_cel_to_far(float(temp_far)):.2f} degrees C")


#Ask user to input a Celsius temperature
temp_cel = input("Enter a temperature in degrees C:  ")

#Print conerted temperature in Fahrenheit
print(f"{temp_cel} degrees C = {convert_far_to_cel(float(temp_cel)):.2f} degrees F")
