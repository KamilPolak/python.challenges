#Farm model


class Animal():
    
    def __init__(self, name, age, weight):
        self.weight = weight
        self.age = age
        self.name = name


    def introduce(self):
        print(f"I am {self.name}, I am {self.age} years old and my weight is {self.weight}")
    
    
class Dog(Animal):

    def __init__(self, name, age, weight, breed):
        super().__init__(name, age, weight)
        self.breed = breed

    def introduce(self):
        super().introduce()
        print(f"I am {self.breed}")

class Pig(Animal):

    def __init__(self, name, age, weight, color):
        super().__init__(name, age, weight)
        self.color = color

    def introduce(self):
        super().introduce()
        print(f"My color is {self.color}")

 

dog = Dog("Tezos", 7, 12, "Pitbull")
pig = Pig("Pepe", 2, 25, "pink")


dog.introduce()
pig.introduce()
